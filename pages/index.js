import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';

export default function Home() {
  const form = useForm();
  const router = useRouter();


  const signUpUser = async (values) => {
    const response = await fetch('https://fjykgnqkkrujgdikatcu.supabase.co/auth/v1/signup', {
      method: 'POST',
      body: JSON.stringify(values),
      headers: {
        apikey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlhdCI6MTY0MjA0NTM2NCwiZXhwIjoxOTU3NjIxMzY0fQ.DOA6GGOhjHWD82MdIAoQNl63R4K1LpodmitxmHMV7eU',
      }
      ,
    })
    const data = await response.json();
    console.log(response);

    if (response.status === 200) {
      router.push('/login');
    }
    else {
      alert(data.msg);
    }
  }

  const handleSignUp = async () => {
    const values = form.getValues();
    await signUpUser(values);
  }

  return (
    <div className='sign-up-wrapper'>
      <div className='sign-up-container-box'>
        <div className='sign-up-content'>
          <h2>Sign Up</h2>
          <div className='email-signup'>
            <label>Email</label>
            <input
              type="email"
              name="email"
              alt='email-id'
              placeholder='Email'
              {...form.register('email')}
            />
          </div>
          <div className='email-signup'>
            <label>Password</label>
            <input type="password"
              alt='password' placeholder='Password'
              autoComplete='new-password'
              name='password'
              {...form.register('password')} />
          </div>
          <div className='sign-up-btn' onClick={handleSignUp}>
            <button>Sign Up</button>
          </div>
        </div>
      </div>
    </div>
  )
}
