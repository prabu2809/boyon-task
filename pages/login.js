import { useRouter } from 'next/router';
import React from 'react';
import { useForm } from 'react-hook-form';


export default function Login() {
    const form = useForm();
    const router = useRouter();

    const loginUser = async (values) => {
        const response = await fetch('https://fjykgnqkkrujgdikatcu.supabase.co/auth/v1/token?grant_type=password', {
            method: 'POST',
            body: JSON.stringify(values),
            headers: {
                apikey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlhdCI6MTY0MjA0NTM2NCwiZXhwIjoxOTU3NjIxMzY0fQ.DOA6GGOhjHWD82MdIAoQNl63R4K1LpodmitxmHMV7eU',
            }
            ,
        })
        const data = await response.json();
        console.log(response);

        if (response.status === 200) {
            router.push('/home');
        }
        else {
            alert(data.msg);
        }
    }

    const handleLogin = async () => {
        const values = form.getValues();
        await loginUser(values);
    }

    return (
        <div className='sign-up-wrapper'>
            <div className='sign-up-container-box'>
                <div className='sign-up-content'>
                    <h2>LOGIN</h2>
                    <div className='email-signup'>
                        <label>Email</label>
                        <input
                            type="email"
                            name="email"
                            alt='email-id'
                            placeholder='Email'
                            {...form.register('email')}
                        />
                    </div>
                    <div className='email-signup'>
                        <label>Password</label>
                        <input type="password"
                            alt='password' placeholder='Password'
                            autoComplete='new-password'
                            name='password'
                            {...form.register('password')} />
                    </div>
                    <div className='sign-up-btn' onClick={handleLogin}>
                        <button>Login</button>
                    </div>
                </div>
            </div>
        </div>
    )
}
