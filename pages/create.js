import { useRouter } from 'next/router';
import React from 'react';
import { useForm } from 'react-hook-form';

export default function Create() {
    const form = useForm();
    const router = useRouter();

    const createBlog = async (values) => {
        console.log(values);
        const response = await fetch('https://fjykgnqkkrujgdikatcu.supabase.co/rest/v1/blog', {
            method: 'POST',
            body: JSON.stringify(values),
            headers: {
                apikey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlhdCI6MTY0MjA0NTM2NCwiZXhwIjoxOTU3NjIxMzY0fQ.DOA6GGOhjHWD82MdIAoQNl63R4K1LpodmitxmHMV7eU',
                Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlhdCI6MTY0MjA0NTM2NCwiZXhwIjoxOTU3NjIxMzY0fQ.DOA6GGOhjHWD82MdIAoQNl63R4K1LpodmitxmHMV7eU',
                "Content-Type": "application/json"
            }
            ,
        })

        router.push('/home');
    }

    const handleCreate = async () => {
        const values = form.getValues();
        if (!(values.user || values.title || values.content)) {
            alert("Please fill the form.");
        }
        else {
            createBlog(values);
        }
    }

    return (
        <div className='create-wrap'>
            <h2>Create Blog</h2>
            <div className='create-card'>
                <div className='create-item'>
                    <label>User</label>
                    <input
                        type="text"
                        name="created_by"
                        alt='user'
                        {...form.register('created_by')}
                    />
                </div>

                <div className='create-item'>
                    <label>Title</label>
                    <input
                        type="text"
                        name="title"
                        alt='title'
                        {...form.register('title')}
                    />
                </div>

                <div className='create-item'>
                    <label>Content</label>
                    <textarea
                        type="text"
                        name="content"
                        alt='content'
                        {...form.register('content')}
                    />
                </div>
                <div className='create-cta-blog' onClick={handleCreate}>
                    <button>Create</button>
                </div>
            </div>
        </div>
    )
}
