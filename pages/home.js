import Router from 'next/router';
import React from 'react';

const home = ({ data }) => {
    const handleCreate = () => {
        Router.push('/create');
    }
    return (
        <div>
            <h2>Blog post title</h2>
            <div className='create-cta' onClick={handleCreate}>
                <button>Create Blog Post</button>
            </div>
            {
                data.map((post) => (
                    <div key={post.id}>
                        <div className="post-list__post post">
                            <div className='blog-card'>
                                <div className='blog-title'>
                                    <div>{post.title}</div>
                                    <div>{post.created_at}</div>
                                </div>
                                <div>
                                    {post.content}
                                </div>
                            </div>
                        </div>
                    </div>
                ))
            }
        </div >
    )
}

export async function getStaticProps() {

    const response = await fetch('https://fjykgnqkkrujgdikatcu.supabase.co/rest/v1/blog?select=*', {
        method: 'GET',
        headers: {
            apikey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlhdCI6MTY0MjA0NTM2NCwiZXhwIjoxOTU3NjIxMzY0fQ.DOA6GGOhjHWD82MdIAoQNl63R4K1LpodmitxmHMV7eU',
            Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlhdCI6MTY0MjA0NTM2NCwiZXhwIjoxOTU3NjIxMzY0fQ.DOA6GGOhjHWD82MdIAoQNl63R4K1LpodmitxmHMV7eU'
        }
        ,
    })
    const data = await response.json();
    console.log(response);

    return {
        props: {
            data,
        },
    }
}

export default home;